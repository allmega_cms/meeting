<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Manager;

use Allmega\MeetingBundle\{Data, Events};
use Allmega\MeetingBundle\Entity\Meeting;
use Allmega\MeetingBundle\Form\MeetingType;
use Allmega\BlogBundle\Events as BlogEvents;
use Allmega\MediaBundle\Events as MediaEvents;
use Allmega\MeetingBundle\Controller\MeetingController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait MeetingControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $this->handleMeeting();
                break;
            case $this->delete:
                $this->clearMeeting();
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $this->handleMeetingFilesAndState();
                $this->params->getEntity()->setUpdated();
                break;
            case $this->delete:
                $this->deleteCommentsAndFiles();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $uid = $this->params->getEntity()->getId();
                $this->params
                    ->setRouteName($this->params->getRouteShort(), $this->show)
                    ->addRouteParams(['id' => $uid, 'type' => 'author']);
                break;
            case $this->delete:
                $this->params->addRouteParams(['id' => 'author']);
                break;
            default:
        }
        return $this;
    }
    
    private function handleMeeting(): void
    {
        $meeting = $this->params->getEntity();
        $action = $this->params->getAction();

        if ($action == $this->add) $meeting->setCreator($this->getUser());

        $active = $meeting->isActive();
        $isActive = $action == $this->state ? !$active : $active;

        $moment = null;
        if ($meeting->getMoment()) $moment = $meeting->getMoment();
        elseif ($isActive && !$meeting->getMoment()) $moment = new \DateTime();

        $meeting
            ->setMoment($moment)
            ->setEditor($this->getUser())
            ->setUpdated();
    }

    private function clearMeeting(): void
    {
        $meeting = $this->params->getEntity();
        $meeting->getMembers()->clear();
        $meeting->getTags()->clear();
    }

    private function handleMeetingFilesAndState(): static
    {
        $meeting = $this->params->getEntity();
        if ($this->params->getAction() != $this->state) {
            $this->saveFiles($meeting->getMediaDir(), $this->getFormFiles());
        }

        if ($meeting->isActive()) {
            $this->params->setEventName(Events::MEETING_ACTIVATED);
            $this->dispatchEvent();
        }
        return $this;
    }

    private function deleteCommentsAndFiles(): void
    {
        $this->params
            ->addArguments(['ids' => $this->getPointsIds()])
            ->setEventName(BlogEvents::DELETE_COMMENTS);
        $this->dispatchEvent();

        $folder = $this->params->getEntity()->getMediaDir();
        $this->params
            ->addArguments(['folder' => $folder])
            ->setEventName(MediaEvents::DELETE_FILES);
        $this->dispatchEvent();
    }

    private function getPointsIds(): array
    {
        $ids = [];
        foreach ($this->params->getEntity()->getPoints() as $point) {
            $ids[] = $point->getId();
        }
        return $ids;
    }

    private function save(Meeting $meeting = null, array $arguments = []): Response
    {
        $eventName = $meeting ? Events::MEETING_UPDATED : Events::MEETING_CREATED;
        $meeting = $meeting ?? new Meeting();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $meeting],
            entity: $meeting,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: MeetingType::class,
            routeName: MeetingController::ROUTE_NAME,
            templatesPath: MeetingController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);      
    }
}