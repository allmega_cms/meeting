<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle;

final class Events
{
    public const MEETING_CREATED = 'meeting.created';
    public const MEETING_UPDATED = 'meeting.updated';
    public const MEETING_DELETED = 'meeting.deleted';
    public const MEETING_ACTIVATED = 'meeting.activated';
    public const MEETING_STATE_CHANGED = 'meeting.state_changed';
    public const POINT_CREATED = 'point.created';
    public const POINT_UPDATED = 'point.updated';
    public const POINT_DELETED = 'point.deleted';
}