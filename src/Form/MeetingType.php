<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Form;

use Allmega\MeetingBundle\Data;
use Allmega\AuthBundle\Model\UsersTrait;
use Allmega\MeetingBundle\Entity\Meeting;
use Allmega\MediaBundle\Form\FileLoadType;
use Allmega\AuthBundle\Repository\UserRepository;
use Allmega\AuthBundle\Utils\Params\UsersTraitParams;
use Allmega\BlogBundle\Form\Type\{TagsInputType, DateTimePickerType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MeetingType extends AbstractType
{
    use UsersTrait;

    public function __construct(
        private readonly UsersTraitParams $usersParams,
        private readonly UserRepository $userRepo) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        extract($this->createOptions());
        $builder
            ->add('title', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'meeting.label.title'],
                'label' => 'meeting.label.title'
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['rows' => 5, 'placeholder' => 'meeting.label.description'],
                'label' => 'meeting.label.description'
            ])
            ->add('moderator', EntityType::class, $moderators)
            ->add('logger', EntityType::class, $loggers)
            ->add('members', EntityType::class, $members)
            ->add('moment', DateTimePickerType::class, [
                'label' => 'meeting.label.moment',
                'help' => 'meeting.help.moment',
                'required' => false
            ])
            ->add('active', null, [
                'label' => 'meeting.label.active',
                'help' => 'meeting.help.active'
            ])
            ->add('url', null, [
                'attr' => ['placeholder' => 'meeting.label.url'],
                'label' => 'meeting.label.url',
                'help' => 'meeting.help.url'
            ])
            ->add('tags', TagsInputType::class, [
                'attr' => ['placeholder' => 'meeting.label.tags'],
                'label' => 'meeting.label.tags',
                'help'  => 'meeting.help.tags',
                'required' => false
            ])
            ->add('files', FileLoadType::class, [
                'attr' => ['multiple' => true],
                'required' => false,
                'mapped' => false,
                'label' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Meeting::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }

    private function createOptions(): array
    {
        $this->usersParams
            ->setLabel('meeting.label.moderator')
            ->setHelp('meeting.help.moderator')
            ->setRoles([Data::USER_ROLE]);

        $moderators = $this->getUsersOptions();

        $this->usersParams
            ->setQueryBuilder(null)
            ->setLabel('meeting.label.logger')
            ->setHelp('meeting.help.logger')
            ->setRoles([Data::USER_ROLE]);

        $loggers = $this->getUsersOptions();

        $this->usersParams
            ->setQueryBuilder(null)
            ->setLabel('meeting.label.members')
            ->setHelp('meeting.help.members')
            ->setRoles([Data::USER_ROLE])
            ->setMultiple(true);

        $members = $this->getUsersOptions();

        return [
            'moderators' => $moderators,
            'loggers' => $loggers,
            'members' => $members
        ];
    }
}