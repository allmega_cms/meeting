<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Repository;

use Allmega\AuthBundle\Entity\User;
use Allmega\MeetingBundle\Entity\Meeting;
use Allmega\BlogBundle\Model\SearchableInterface;
use Allmega\BlogBundle\Utils\Params\{RepoParams, UTypes};
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\{QueryBuilder, Query};

class MeetingRepository extends ServiceEntityRepository implements SearchableInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Meeting::class);
    }

    public function findAllQuery(RepoParams $params): Query
    {
        $query = $this->createQueryBuilder('a');

        if ($params->getCount()) $query->select('COUNT(a.id)');
        elseif ($params->getLimit() > 0) $query->setMaxResults($params->getLimit());

        $this->modifyQuery($query, $params->getType(), $params->getUser()->getId());
        return $query->orderBy('a.created', 'DESC')->getQuery();
    }

    public function findByRepoParams(RepoParams $params): int|array
    {
        $query = $this->findAllQuery($params);
        return $params->getCount() ? $query->getSingleScalarResult() : $query->getResult();
    }

    public function countLatest(User $user): int
    {
        return $this->createQueryBuilder('m')
            ->select('COUNT(m.id)')
            ->join('m.members', 'u')
            ->where('u.id = :id')
            ->andWhere('m.created BETWEEN :last AND :now')
            ->setParameter('last', $user->getLastSeen()->format('Y-m-d H:i:S'))
            ->setParameter('now', (new \DateTime())->format('Y-m-d H:i:s'))
            ->setParameter('id', $user->getId())
            ->andWhere('m.active = 1')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (count($terms) < 1) return [];
        
        extract($options);
        //[$user, $type]

        $query = $this->createQueryBuilder('m');

        foreach ($terms as $key => $term) {
            $query
                ->orWhere('m.title LIKE :t_'.$key)
                ->setParameter('t_'.$key, '%'.$term.'%')
                ->orWhere('m.description LIKE :d_'.$key)
                ->setParameter('d_'.$key, '%'.$term.'%');
        }

        $this->modifyQuery($query, $type, $user()->getId());
        return $query
            ->orderBy('m.title', 'ASC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    private function modifyQuery(QueryBuilder &$query, string $type, string $uid): void
    {
        switch ($type) {
            case UTypes::AUTHOR:
                $query
                    ->join('a.creator', 'u')
                    ->where('u.id = :id')
                    ->setParameter('id', $uid);
                break;
            case UTypes::MEMBER:
                $query
                    ->join('a.members', 'u')
                    ->where('u.id = :id')
                    ->setParameter('id', $uid)
                    ->andWhere('a.active = 1');
                break;
            default:
        }
    }
}