<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Entity;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\Helper;
use Allmega\MeetingBundle\Model\BaseProps;
use Allmega\MeetingBundle\Repository\PointRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PointRepository::class)]
#[ORM\Table(name: '``allmega_meeting__point`')]
class Point extends BaseProps
{
    #[ORM\ManyToOne(targetEntity: Meeting::class, inversedBy: 'points')]
    private ?Meeting $meeting;

    /**
     * Create a new Point entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $title, $description as dummy text
     * - $meeting, $user will be created
     */
    public static function build(
        string $description = null,
        Meeting $meeting = null,
        string $title = null,
        User $user = null): static
    {
        $user = $user ?? User::build();
        $meeting = $meeting ?? Meeting::build(user: $user);
        $description = $description ?? Helper::generateRandomString();
        $title = $title ?? Helper::generateRandomString();

        return (new static())
            ->setDescription($description)
            ->setMeeting($meeting)
            ->setCreator($user)
            ->setEditor($user)
            ->setTitle($title);
    }

    public function getMeeting(): ?Meeting
    {
        return $this->meeting;
    }

    public function setMeeting(?Meeting $meeting): static
    {
        $this->meeting = $meeting;
        return $this;
    }

    public function setMediaDir(): void
    {
        $this->mediaDir = $this->meeting->getMediaDir().'_points_'.$this->id;
    }
}