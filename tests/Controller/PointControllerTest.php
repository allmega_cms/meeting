<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Tests\Controller;

use Allmega\MeetingBundle\Controller\{MeetingController, PointController};
use Allmega\MeetingBundle\Entity\{Meeting, Point};
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\MeetingBundle\Data;

class PointControllerTest extends AllmegaWebTest
{
    public function testShow(): void
    {
        $this->runTests($this->edit, Data::USER_ROLE, true);
    }

    public function testAdd(): void
    {
        $id = $this->createMeeting()->getId();
        $this->runTests(
            routeParams: ['id' => $id],
            role: Data::AUTHOR_ROLE,
            routeName: $this->add);
    }

    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::AUTHOR_ROLE, true);
    }

    public function testDelete(): void
    {
        $redirectRoute = MeetingController::ROUTE_NAME . $this->show;
        $this->runModifyTests(
            fnRedirectParams: 'getRouteParams',
            redirectRoute: $redirectRoute,
            role: Data::AUTHOR_ROLE,
            delete: true);
    }

    protected function create(): void
    {
        $meeting = $this->createMeeting();
        $entity = Point::build(meeting: $meeting, user: $meeting->getCreator());
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return PointController::ROUTE_NAME . $name;
    }

    private function createMeeting(): Meeting
    {
        $user = $this->findUserByRole(Data::AUTHOR_ROLE);
        $meeting = Meeting::build(user: $user);
        $this->em->persist($meeting);
        $this->em->flush();
        return $meeting;
    }

    protected function getRouteParams(): array
    {
        return ['id' => $this->params->getEntity()->getMeeting()->getId()];
    }
}