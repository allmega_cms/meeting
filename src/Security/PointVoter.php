<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Security;

use Allmega\MeetingBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\MeetingBundle\Utils\SecurityInfo;
use Allmega\MeetingBundle\Entity\{Meeting, Point};
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PointVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'meeting-point');
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        $isUser = $this->hasRole($user, Data::USER_ROLE);
        if (!$isUser || !$this->isSettedAndSupports($attribute, $subject)) return false;

        $securityInfo = $this->getSecurityInfo($user, $subject);

        switch($attribute) {
            case $this->add:
                $isAuthor = $securityInfo->isAuthor() && $securityInfo->isMember();
                $granted = $securityInfo->isManager() || $isAuthor;
                $result = $securityInfo->isMeeting() && $granted;
                break;
            case $this->show:
                $granted = $securityInfo->isManager() || $securityInfo->isMember();
                $result = $securityInfo->isPoint() && $granted;
                break;
            case $this->delete:
            case $this->edit:
                $granted = $securityInfo->isManager() || $securityInfo->isLogger() || $securityInfo->isSameUser();
                $result = $securityInfo->isPoint() && $granted;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Meeting || $subject instanceof Point;
    }

    private function getSecurityInfo(User $user, mixed $subject): SecurityInfo
    {
        $securityInfo = (new SecurityInfo())
            ->setManager($this->hasRole($user, Data::MANAGER_ROLE))
            ->setAuthor($this->hasRole($user, Data::AUTHOR_ROLE));

        if ($subject) {
            $isMeeting = $subject instanceof Meeting;
            $isPoint = $subject instanceof Point;

            $isSameUser = $this->isSameUser($user, $subject->getCreator());

            $isMember = $isPoint ?
                $subject->getMeeting()->getMembers()->contains($user) :
                $subject->getMembers()->contains($user);

            $isModerator = $isPoint ?
                $this->isSameUser($user, $subject->getMeeting()->getModerator()) :
                $this->isSameUser($user, $subject->getModerator());

            $isLogger = $isPoint ?
                $this->isSameUser($user, $subject->getMeeting()->getLogger()) :
                $this->isSameUser($user, $subject->getLogger());

            $securityInfo
                ->setModerator($isModerator)
                ->setSameUser($isSameUser)
                ->setMeeting($isMeeting)
                ->setLogger($isLogger)
                ->setMember($isMember)
                ->setPoint($isPoint);
        }
        return $securityInfo;
    }
}