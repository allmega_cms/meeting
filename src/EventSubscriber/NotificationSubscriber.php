<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\EventSubscriber;

use Allmega\MeetingBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Params\NotificationParams;
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class NotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(private MessageBusInterface $bus) {}

    public static function getSubscribedEvents(): array
    {
        return [
            Events::MEETING_ACTIVATED => 'onMeetingActivated',
        ];
    }

    /**
     * @throws ExceptionInterface
     */
    public function onMeetingActivated(GenericEvent $event): void
    {
        $meeting = $event->getSubject();
        $search  = ['%title%', '%date%', '%desc%'];
        $replace = [
            $meeting->getTitle(),
            $meeting->getMoment()->format('d.m.Y H:i'),
            $meeting->getDescription()
        ];

        $params = new NotificationParams(Data::NOTIFICATION_MEETING_ENABLED, [], $search, $replace);
        foreach ($meeting->getMembers() as $user) $params->addReceiver($user->getEmail());
        $this->bus->dispatch($params);
    }
}