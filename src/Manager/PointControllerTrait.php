<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Manager;

use Allmega\MeetingBundle\{Data, Events};
use Allmega\MeetingBundle\Form\PointType;
use Allmega\BlogBundle\Events as BlogEvents;
use Allmega\MediaBundle\Events as MediaEvents;
use Allmega\MeetingBundle\Entity\{Meeting, Point};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\MeetingBundle\Controller\{MeetingController, PointController};
use Symfony\Component\HttpFoundation\Response;

trait PointControllerTrait
{
    protected function preExecution(): static
    {
        $point = $this->params->getEntity();
        $user = $this->getUser();

        switch ($this->params->getAction()) {
            case $this->add:
                $point->setCreator($user)->setEditor($user);
                break;
            case $this->edit:
                $point->setEditor($user)->setUpdated();
                break;
            default:
        }
        return $this;
    }
    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $mediaDir = $this->params->getEntity()->getMediaDir();
                $this->saveFiles($mediaDir, $this->getFormFiles());
                break;
            case $this->delete:
                $this->deleteCommentsAndFiles();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $id = $this->params->getEntity()->getId();
                $this->params
                    ->setRouteName($this->params->getRouteShort(), $this->show)
                    ->addRouteParams(['id' => $id]);
                break;
            case $this->delete:
                $id = $this->params->getEntity()->getMeeting()->getId();
                $this->params
                    ->setRouteName(MeetingController::ROUTE_NAME, $this->show)
                    ->addRouteParams(['id' => $id]);
                break;
            default:
        }
        return $this;
    }

    private function deleteCommentsAndFiles(): void
    {
        $id = $this->params->getEntity()->getId();
        $this->params
            ->addArguments(['ids' => [$id]])
            ->setEventName(BlogEvents::DELETE_COMMENTS);
        $this->dispatchEvent();

        $folder = $this->params->getEntity()->getMediaDir();
        $this->params
            ->addArguments(['folder' => $folder])
            ->setEventName(MediaEvents::DELETE_FILES);
        $this->dispatchEvent();
    }

    private function save(Point $point = null, Meeting $meeting = null, array $arguments = []): Response
    {
        $item = $point ?? $meeting;
        $meeting = $meeting ?? $point->getMeeting();
        $formParams = $this->buildFormParams($meeting, $item);

        $eventName = $point ? Events::POINT_UPDATED : Events::POINT_CREATED;
        $point = $point ?? (new Point())->setMeeting($meeting);

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            entity: $point,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: PointType::class,
            routeName: PointController::ROUTE_NAME,
            templatesPath: PointController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }

    private function buildFormParams(Meeting $meeting, Meeting|Point $item): array
    {
        return [
            'link' => [
                'route' => MeetingController::ROUTE_NAME . 'show',
                'params' => ['id' => $meeting->getId()],
                'title' => 'meeting.show',
            ],
            'item' => $item
        ];
    }
}