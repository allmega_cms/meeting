<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Form;

use Allmega\MeetingBundle\Data;
use Allmega\MeetingBundle\Entity\Point;
use Allmega\MediaBundle\Form\FileLoadType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PointType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'meeting.label.title'],
                'label' => 'meeting.label.title'
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['rows' => 5, 'placeholder' => 'meeting.label.description'],
                'label' => 'meeting.label.description'
            ])
            ->add('files', FileLoadType::class, [
                'attr' => ['multiple' => true],
                'required' => false,
                'mapped' => false,
                'label' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Point::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}