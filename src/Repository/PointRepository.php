<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Repository;

use Allmega\MeetingBundle\Entity\Point;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class PointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Point::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')->orderBy('a.created', 'DESC')->getQuery();
    }

    public function findByCreatorIdQuery(string $uid): Query
    {
        return $this->createQueryBuilder('a')
            ->join('a.creator', 'u')
            ->where('u.id = :uid')
            ->orderBy('a.created', 'DESC')
            ->setParameter('uid', $uid)
            ->getQuery();
    }
}