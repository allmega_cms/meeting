<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Security;

use Allmega\MeetingBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\MeetingBundle\Entity\Meeting;
use Allmega\MeetingBundle\Utils\SecurityInfo;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MeetingVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'meeting');
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        $isUser = $this->hasRole($user, Data::USER_ROLE);
        if (!$isUser || !$this->isSettedAndSupports($attribute, $subject)) return false;

        $securityInfo = $this->getSecurityInfo($user, $subject);

        switch($attribute) {
            case $this->dashboard:
            case $this->search:
            case $this->list:
                $result = true;
                break;
            case $this->show:
                $result = $securityInfo->isManager() || $securityInfo->isMember();
                break;
            case $this->add:
                $granted = $securityInfo->isMember() && $securityInfo->isAuthor();
                $result = $securityInfo->isManager() || $granted;
                break;
            case $this->delete:
            case $this->state:
            case $this->edit:
                $result = $securityInfo->isManager() || $securityInfo->isModerator() || $securityInfo->isSameUser();;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Meeting;
    }

    private function getSecurityInfo(User $user, mixed $subject): SecurityInfo
    {
        $securityInfo = (new SecurityInfo())
            ->setManager($this->hasRole($user, Data::MANAGER_ROLE))
            ->setAuthor($this->hasRole($user, Data::AUTHOR_ROLE));

        if ($subject) {
            $isModerator = $this->isSameUser($user, $subject->getModerator());
            $isSameUser = $this->isSameUser($user, $subject->getCreator());
            $isLogger = $this->isSameUser($user, $subject->getLogger());
            $isMember = $subject->getMembers()->contains($user);

            $securityInfo
                ->setModerator($isModerator)
                ->setSameUser($isSameUser)
                ->setLogger($isLogger)
                ->setMember($isMember);
        }
        return $securityInfo;
    }
}