<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle;

use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntriesMap, ControllerEntry};
use Allmega\MeetingBundle\Controller\{MeetingController, PointController};
use Allmega\BlogBundle\Utils\Loader\Entries\MenupointEntry;
use Allmega\BlogBundle\Model\PackageData;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\MeetingBundle\Entity\Point;
use Allmega\BlogBundle\Entity\Item;

class Data extends PackageData
{
	public const DOMAIN = 'AllmegaMeetingBundle';
	public const PACKAGE = 'meeting';

    public const NOTIFICATION_MEETING_ENABLED = 'meeting_enabled';
    public const NOTIFICATION_POINT_CREATED = 'meeting_point_created';

	public const GROUP_TYPE_MEETING = 'meeting.main';

	public const MANAGER_GROUP = 'meeting.manager';
	public const AUTHOR_GROUP = 'meeting.author';
    public const USER_GROUP = 'meeting.user';

	public const MANAGER_ROLE = 'ROLE_MEETING_MANAGER';
	public const AUTHOR_ROLE = 'ROLE_MEETING_AUTHOR';
	public const USER_ROLE = 'ROLE_MEETING_USER';

	protected function setRegisterData(): void
	{
        $this->package = self::PACKAGE;
        $this->data = [
            'controllerEntries' => $this->getControllerEntries(),
            'webpackEntries' => $this->getWebpackEntries(),
        ];
	}

    protected function setLoadData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'notificationtypes' => $this->getNotificationTypes(),
            'menupoints' => $this->getMenuPoints(),
            'grouptypes' => $this->getGroupTypes(),
            'groups' => $this->getGroups(),
            'roles' => $this->getRoles(),
            'users' => $this->getUsers(),
            'items' => $this->getItems(),
        ];
    }

	/**
	 * @return array<int,ControllerEntry>
	 */
	protected function getControllerEntries(): array
	{
		return [
			new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'meeting',
                MeetingController::class,
                'getDashboardWidget'),
		];
	}

	/**
	 * @return array<int,MenupointEntry>
	 */
	protected function getMenuPoints(): array
	{
        $routeName = MeetingController::ROUTE_NAME . 'index';
		$main = 'meeting.main';
		$menuPoints = [
            new MenupointEntry('meeting.manager', $routeName, ['id' => 'manager'], $this->routeType, 3, [self::MANAGER_GROUP], $main),
			new MenupointEntry('meeting.author', $routeName, ['id' => 'author'], $this->routeType, 2, [self::AUTHOR_GROUP], $main),
			new MenupointEntry('meeting.member', $routeName, [], $this->routeType, 1, [self::USER_GROUP], $main),
			new MenupointEntry($main, '', [], $this->menuType, 7, [], BlogData::MENUPOINT_BLOG_MANAGE),
		];
		return [self::PACKAGE => $this->setSysAndActive($menuPoints)];
	}

	/**
	 * @return array<int,Item>
	 */
	protected function getItems(): array
	{
		$items = [
			Item::build(
                Point::class,
                PointController::ROUTE_NAME . 'show',
                'point.item.description',
                'point.item.label'),
		];
		return [self::PACKAGE => $items];
	}

    protected function getAuthData(): array
    {
        return [
            self::GROUP_TYPE_MEETING => [
                self::MANAGER_GROUP => [
                    self::MANAGER_ROLE => 'meeting.manager',
                    self::AUTHOR_ROLE => 'meeting.author',
                    self::USER_ROLE => 'meeting.user',
                ],
                self::AUTHOR_GROUP => [
                    self::AUTHOR_ROLE => 'meeting.author',
                    self::USER_ROLE => 'meeting.user',
                ],
                self::USER_GROUP => [
                    self::USER_ROLE => 'meeting.user',
                ],
            ],
        ];
    }

    protected function getNotificationTypesData(): array
    {
        return [
            self::NOTIFICATION_MEETING_ENABLED,
            self::NOTIFICATION_POINT_CREATED,
        ];
    }
}