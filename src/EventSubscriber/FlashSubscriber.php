<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\EventSubscriber;

use Allmega\MeetingBundle\Events;
use Allmega\BlogBundle\Model\FlashesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlashSubscriber implements EventSubscriberInterface
{
    use FlashesTrait;

    public static function getSubscribedEvents(): array
    {
        return [
            Events::MEETING_CREATED => 'addSuccessFlash',
            Events::MEETING_UPDATED => 'addSuccessFlash',
            Events::MEETING_DELETED => 'addSuccessFlash',
            Events::MEETING_ACTIVATED => 'addSuccessFlash',
            Events::MEETING_STATE_CHANGED => 'addSuccessFlash',
            Events::POINT_CREATED => 'addSuccessFlash',
            Events::POINT_UPDATED => 'addSuccessFlash',
            Events::POINT_DELETED => 'addSuccessFlash',
        ];
    }
}