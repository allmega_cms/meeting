<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Controller;

use Allmega\MeetingBundle\{Data, Events};
use Allmega\MeetingBundle\Security\PointVoter;
use Allmega\MeetingBundle\Entity\{Point, Meeting};
use Allmega\MeetingBundle\Manager\PointControllerTrait;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/point', name: 'allmega_meeting_point_')]
class PointController extends BaseController
{
    use PointControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaMeeting/point/';
    public const ROUTE_NAME = 'allmega_meeting_point_';
    public const PROP = 'point';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/show/{id}', name: 'show', methods: 'GET')]
    #[IsGranted('meeting-point-show', subject: self::PROP)]
    public function show(PointVoter $pointVoter, Point $point): Response
    {
        $optParams = [
            'entityname' => Point::class,
            'pointVoter' => $pointVoter,
            'data' => ['show' => false],
            'meeting' => [
                'route' => MeetingController::ROUTE_NAME
            ]
        ];
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'point' => $point
        ]);
    }

    #[Route('/add/{id}', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('meeting-point-add', subject: 'meeting')]
    public function add(Meeting $meeting): Response
    {
        return $this->save(null, $meeting);
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('meeting-point-edit', subject: self::PROP)]
    public function edit(Point $point): Response
    {
        return $this->save($point);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('meeting-point-delete', subject: self::PROP)]
    public function delete(Point $point): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $point,
            domain: Data::DOMAIN,
            eventName: Events::POINT_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }
}