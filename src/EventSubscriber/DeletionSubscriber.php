<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\EventSubscriber;

use Allmega\BlogBundle\Entity\Tag;
use Allmega\AuthBundle\Entity\User;
use Allmega\AuthBundle\Events as AuthEvents;
use Allmega\BlogBundle\Events as BlogEvents;
use Allmega\MeetingBundle\Entity\{Meeting, Point};
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Doctrine\ORM\EntityManagerInterface;

readonly class DeletionSubscriber implements EventSubscriberInterface
{
    public function __construct(private EntityManagerInterface $em) {}

    public static function getSubscribedEvents(): array
    {
        return [
            AuthEvents::USER_DELETE => 'handleUserRelations',
            BlogEvents::TAG_DELETED => 'handleTagRelations',
        ];
    }

    public function handleUserRelations(GenericEvent $event, string $eventName): void
    {
        $userRepo = $this->em->getRepository(User::class);
        $user = $event->getSubject();

        $userRepo
            ->updatefield(Meeting::class, 'moderator', $user)
            ->updatefield(Meeting::class, 'creator', $user)
            ->updatefield(Meeting::class, 'editor', $user)
            ->updatefield(Meeting::class, 'logger', $user)
            ->updateField(Point::class, 'creator', $user)
            ->updateField(Point::class, 'editor', $user)
            ->deleteUserRelations('allmega_meeting__user', $user);
    }

    public function handleTagRelations(GenericEvent $event, string $eventName): void
    {
        $this->em->getRepository(Tag::class)->deleteTagRelations('allmega_meeting__tag', $event);
    }
}