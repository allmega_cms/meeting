<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Utils;

class SecurityInfo
{
    public function __construct (
        private bool $moderator = false,
        private bool $sameUser = false,
        private bool $manager = false,
        private bool $author = false,
        private bool $member = false,
        private bool $logger = false,
        private bool $meeting = false,
        private bool $point = false) {}

    public function isModerator(): bool
    {
        return $this->moderator;
    }

    public function setModerator(bool $moderator): static
    {
        $this->moderator = $moderator;
        return $this;
    }

    public function isSameUser(): bool
    {
        return $this->sameUser;
    }

    public function setSameUser(bool $sameUser): static
    {
        $this->sameUser = $sameUser;
        return $this;
    }

    public function isManager(): bool
    {
        return $this->manager;
    }

    public function setManager(bool $manager): static
    {
        $this->manager = $manager;
        return $this;
    }

    public function isAuthor(): bool
    {
        return $this->author;
    }

    public function setAuthor(bool $author): static
    {
        $this->author = $author;
        return $this;
    }

    public function isMember(): bool
    {
        return $this->member;
    }

    public function setMember(bool $member): static
    {
        $this->member = $member;
        return $this;
    }

    public function isLogger(): bool
    {
        return $this->logger;
    }

    public function setLogger(bool $logger): static
    {
        $this->logger = $logger;
        return $this;
    }

    public function isMeeting(): bool
    {
        return $this->meeting;
    }

    public function setMeeting(bool $meeting): static
    {
        $this->meeting = $meeting;
        return $this;
    }

    public function isPoint(): bool
    {
        return $this->point;
    }

    public function setPoint(bool $point): static
    {
        $this->point = $point;
        return $this;
    }
}