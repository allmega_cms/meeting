<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Entity;

use Allmega\BlogBundle\Entity\Tag;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\Helper;
use Allmega\MeetingBundle\Model\BaseProps;
use Allmega\BlogBundle\Model\SortableItemInterface;
use Allmega\MeetingBundle\Repository\MeetingRepository;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;

#[ORM\Entity(repositoryClass: MeetingRepository::class)]
#[ORM\Table(name: '`allmega_meeting`')]
class Meeting extends BaseProps implements SortableItemInterface
{
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $moment = null;

    #[ORM\Column(length: 191, nullable: true)]
    private ?string $url = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;

    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: '`allmega_meeting__user`')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    private Collection $members;

    #[ORM\ManyToOne]
    private ?User $moderator;

    #[ORM\ManyToOne]
    private ?User $logger;

    #[Assert\Count(max: 4, maxMessage: 'errors.too_many_tags')]
    #[ORM\ManyToMany(targetEntity: Tag::class, cascade: ['persist'])]
    #[ORM\JoinTable(name: '`allmega_meeting__tag`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $tags;

    #[ORM\OneToMany(targetEntity: Point::class, mappedBy: 'meeting', orphanRemoval: true)]
    #[ORM\OrderBy(['title' => 'ASC'])]
    private Collection $points;

    /**
     * Create a new Meeting entity with predetermined data, 
     * if no data is provided, it will be generated:
     * - $title, $description as dummy text
     * - $active will be set to false
     * - $user will be created
     */
    public static function build(
        string $description = null,
        string $title = null,
        bool $active = false,
        User $user = null): static
    {
        $description = $description ?? Helper::generateRandomString();
        $title = $title ?? Helper::generateRandomString();
        $user = $user ?? User::build();

        return (new static())
            ->setDescription($description)
            ->setCreator($user)
            ->setEditor($user)
            ->setActive($active)
            ->setTitle($title);
    }

    public function __construct()
    {
        parent::__construct();
        $this->members = new ArrayCollection();
        $this->points = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getMoment(): ?DateTimeInterface
    {
        return $this->moment;
    }

    public function setMoment(?DateTimeInterface $moment): static
    {
        $this->moment = $moment;
        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): static
    {
        $this->url = $url;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return Collection<int,User>
     */
    public function getMembers(): Collection
    {
        return $this->members;
    }

    public function addMember(User $member): static
    {
        if (!$this->members->contains($member)) $this->members[] = $member;
        return $this;
    }

    public function removeMember(User $member): static
    {
        if ($this->members->contains($member)) $this->members->removeElement($member);
        return $this;
    }

    public function getModerator(): ?User
    {
        return $this->moderator;
    }

    public function setModerator(?User $moderator): static
    {
        $this->moderator = $moderator;
        return $this;
    }

    public function getLogger(): ?User
    {
        return $this->logger;
    }

    public function setLogger(?User $logger): static
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return Collection<int,Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag ...$tags): void
    {
        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) $this->tags->add($tag);
        }
    }

    public function removeTag(Tag $tag): void
    {
        $this->tags->removeElement($tag);
    }

    /**
     * @return Collection<int,Point>
     */
    public function getPoints(): Collection
    {
        return $this->points;
    }

    public function addPoint(Point $point): static
    {
        if (!$this->points->contains($point)) {
            $this->points[] = $point;
            $point->setMeeting($this);
        }
        return $this;
    }

    public function removePoint(Point $point): static
    {
        if ($this->points->contains($point)) {
            $this->points->removeElement($point);
            if ($point->getMeeting() === $this) $point->setMeeting(null);
        }
        return $this;
    }

    public function setMediaDir(): void
    {
        $this->mediaDir = 'meetings_'.$this->id;
    }

    public static function getSortableProps(): array
    {
        return ['title', 'moment', 'created', 'updated', 'active'];
    }

    public static function getBundleName(): string
    {
        return 'Meeting';
    }
}