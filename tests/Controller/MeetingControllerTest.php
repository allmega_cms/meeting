<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Tests\Controller;

use Allmega\MeetingBundle\Controller\MeetingController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\MeetingBundle\Entity\Meeting;
use Allmega\MeetingBundle\Data;

class MeetingControllerTest extends AllmegaWebTest
{
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, Data::USER_ROLE, false, false);
    }

    public function testSearch(): void
    {
        $this->runTests($this->search, Data::USER_ROLE);
    }

    public function testIndex(): void
    {
        $this->runTests($this->index, Data::USER_ROLE);
    }

    public function testShow(): void
    {
        $this->runTests($this->show, Data::USER_ROLE, true);
    }

    public function testAdd(): void
    {
        $this->runTests($this->add, Data::AUTHOR_ROLE);
    }

    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::AUTHOR_ROLE, true);
    }

    public function testDelete(): void
    {
        $this->runModifyTests(
            redirectParams: ['id' => 'author'],
            role: Data::AUTHOR_ROLE,
            delete: true);
    }

    public function testState(): void
    {
        $this->runModifyTests(
            fnRedirectParams: 'getRouteParams',
            role: Data::AUTHOR_ROLE);
    }

    protected function create(): void
    {
        $user = $this->findUserByRole(Data::AUTHOR_ROLE);
        $entity = Meeting::build(user: $user);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return MeetingController::ROUTE_NAME . $name;
    }
    
    protected function getRouteParams(): array
    {
        return ['id' => $this->params->getEntity()->getId(), 'type' => 'author'];
    }
}