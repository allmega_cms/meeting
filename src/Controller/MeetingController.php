<?php

/**
 * This file is part of the Allmega Meeting Bundle package.
 *
 * @copyright Allmega 
 * @package   Meeting Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\MeetingBundle\Controller;

use Allmega\MeetingBundle\{Data, Events};
use Allmega\MeetingBundle\Entity\Meeting;
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\BlogBundle\Utils\Params\TemplateParams;
use Allmega\MeetingBundle\Repository\MeetingRepository;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\MeetingBundle\Manager\MeetingControllerTrait;
use Allmega\MeetingBundle\Security\{MeetingVoter, PointVoter};
use Allmega\BlogBundle\Utils\Params\{BaseControllerParams, RepoParams, UTypes};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(name: 'allmega_meeting_')]
class MeetingController extends BaseController
{
    use MeetingControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaMeeting/meeting/';
    public const ROUTE_NAME = 'allmega_meeting_';
    public const PROP = 'meeting';

    public function __construct(
        private readonly MeetingRepository $meetingRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search/{type}', name: 'search', requirements: ['type' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('meeting-search')]
    public function search(SearchItem $item, string $type = UTypes::MEMBER): Response
    {
        $type = $this->checkType($type);
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setRouteParams(['type' => $type])
            ->setProps(['title'])
            ->getRepo()
            ->setSearch($this->meetingRepo)
            ->addOptions(['type' => $type, 'user' => $this->getUser()]);
        
        $item->getTrans()->setLabel('meeting.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list/{id}', name: 'index', requirements: ['id' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('meeting-list')]
    public function index( MeetingVoter $meetingVoter, Paginator $paginator, string $id = UTypes::MEMBER): Response
    {
        $type = $this->checkType($id);
        $repoParams = new RepoParams($this->getUser(), $type);
        $query = $this->meetingRepo->findAllQuery($repoParams);

        $optParams = [
            'data' => new TemplateParams(self::PROP, $type),
            'meetingVoter' => $meetingVoter,
        ];

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'item' => SortableItem::getInstance(new Meeting()),
            'meetings' => $paginator->getPagination($query),
        ]);
    }

    #[Route('/show/{id}/{type}', name: 'show', requirements: ['type' => UTypes::TYPES], methods: 'GET')]
    #[IsGranted('meeting-show', subject: self::PROP)]
    public function show(
        MeetingVoter $meetingVoter,
        PointVoter $pointVoter,
        Meeting $meeting, 
        string $type = UTypes::MEMBER): Response
    {
        $type = $this->checkType($type);
        $optParams = [
            'data' => new TemplateParams(self::PROP, $type, true),
            'meetingVoter' => $meetingVoter,
            'pointVoter' => $pointVoter,
            'point' => [
                'path' => PointController::ROUTE_TEMPLATE_PATH,
                'route' => PointController::ROUTE_NAME
            ]
        ];

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'meeting' => $meeting
        ]);
    }

   #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
   #[IsGranted('meeting-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: 'GET|POST')]
    #[IsGranted('meeting-edit', subject: self::PROP)]
    public function edit(Meeting $meeting): Response
    {
        return $this->save($meeting);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('meeting-delete', subject: self::PROP)]
    public function delete(Meeting $meeting): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $meeting,
            domain: Data::DOMAIN,
            eventName: Events::MEETING_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('meeting-state', subject: self::PROP)]
    public function changeState(Meeting $meeting): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $meeting,
            domain: Data::DOMAIN,
            eventName: Events::MEETING_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(MeetingRepository $meetingRepo): Response
    {
        if (!$this->isGranted('meeting-dashboard')) return new Response();
        
        $user = $this->getUser();
        $latestNum = $meetingRepo->countLatest($user);

        $repoParams = new RepoParams($user, count: true);
        $num = $meetingRepo->findByRepoParams($repoParams);

        $repoParams->setCount(false)->setLimit(3);
        $meetings = $meetingRepo->findByRepoParams($repoParams);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'meetings' => $meetings, 'latestNum' => $latestNum, 'num' => $num,
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }

    private function checkType(string $type): string
    {
        if ($type == UTypes::MANAGER && $this->isGranted(Data::MANAGER_ROLE)) {
            return $type;
        } elseif ($type == UTypes::AUTHOR && $this->isGranted(Data::AUTHOR_ROLE)) {
            return $type;
        } else {
            return UTypes::MEMBER;
        }
    }
}